import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
from decimal import Decimal
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from sklearn.neighbors import KNeighborsClassifier
from time import sleep

def init_and_get_driver(site):
    driver = webdriver.PhantomJS()
    driver.set_window_size(1120, 550)
    driver.get(site)
    return driver

def get_age_group_list(tr):
    age_group_list = list()
    select = tr.find_element_by_name('PDim_AGECODE')
    options = select.find_elements(By.TAG_NAME, 'option')
    for i in range(1, len(options)):
        age_group_list.append((options[i].get_attribute('value'), options[i].text))
    return age_group_list

def get_gender_list(tr):
    gender_list = list()
    select = tr.find_element_by_name('PDim_SEXCODE')
    options = select.find_elements(By.TAG_NAME, 'option')
    for i in range(1, len(options)):
        gender_list.append((options[i].get_attribute('value'), options[i].text))
    return gender_list

def get_ethnicity_list(tr):
    ethnicity_list = list()
    select = tr.find_element_by_name('PDim_ETHNICITYCODE')
    options = select.find_elements(By.TAG_NAME, 'option')
    for i in range(1, len(options)):
        ethnicity_list.append((options[i].get_attribute('value'), options[i].text))
    return ethnicity_list

def get_year_month_list(tr):
    year_month_list = list()
    ths = tr.find_elements_by_class_name('HDim')
    for th in ths:
        year_month_list.append(th.text)
    return year_month_list

def get_column_headers(driver):
    columns = dict()
    dataTables = driver.find_elements_by_class_name('DataTable')
    thead = dataTables[0].find_element(By.TAG_NAME, 'thead')
    trs = thead.find_elements(By.TAG_NAME, 'tr')
    columns['AGEGRP'] = get_age_group_list(trs[1])
    columns['GENDER'] = get_gender_list(trs[2])
    columns['ETHNICITY'] = get_ethnicity_list(trs[3])
    columns['YEARMONTH'] = get_year_month_list(trs[6])
    return columns

def select_age(driver, age):
    while True:
        try:
            select = Select(driver.find_element_by_name("PDim_AGECODE"))
            break
        except NoSuchElementException:
            sleep(0.5)
    select.select_by_value(age)

def select_gender(driver, gender):
    while True:
        try:
            select = Select(driver.find_element_by_name("PDim_SEXCODE"))
            break
        except NoSuchElementException:
            sleep(0.5)
    select.select_by_value(gender)

def select_ethnicity(driver, ethnicity):
    while True:
        try:
            select = Select(driver.find_element_by_name("PDim_ETHNICITYCODE"))
            break
        except NoSuchElementException:
            sleep(0.5)
    select.select_by_value(ethnicity)

def get_table(driver):
    while True:
        try:
            table = driver.find_element_by_id('tabletofreeze')
            return table
        except NoSuchElementException:
            sleep(0.5)

def do_web_scrape(driver, columns):
	age_groups = columns['AGEGRP']
	genders = columns['GENDER']
	ethnicities = columns['ETHNICITY']
	year_month = columns['YEARMONTH']
	loopcount = 0
	df = pd.DataFrame(columns = {'CRIME', 'AGEGRP', 'GENDER', 'ETHNICITY', 'YEAR', 'COUNT'})
	df_idx = 0
	for age_group in age_groups:
		select_age(driver, age_group[0])
		sleep(1)
		for ethnicity in ethnicities:
			select_ethnicity(driver, ethnicity[0])
			sleep(1)
			for gender in genders:
				select_gender(driver, gender[0])
				sleep(1)
				table = get_table(driver)
				tbody = table.find_element(By.TAG_NAME, 'tbody')
				trs = tbody.find_elements(By.TAG_NAME, 'tr')
				for tr in trs:
					tds = tr.find_elements(By.TAG_NAME, 'td')
					for i in range(2, len(tds)):
						row = list()
						if tds[i].text == '..':
							row.append(0)
						else:
							row.append(Decimal(tds[i].text.replace(",","")))
							row_label = tds[0].text
							row.append(age_group[1])
							row.append(gender[1])
							row.append(row_label)
							row.append(year_month[i-2])
							row.append(ethnicity[1])
							df.loc[df_idx] = row
							df_idx += 1
	return df

def map_labels_to_values(value_list):
	value_map = list(tuple())
	for i in range(len(value_list)):
		value_map.append((i, value_list[i]))
	return value_map

def get_population_data(driver):
	while True:
		try:
			textBox = driver.find_element_by_id('ctl00_MainContent_ctlSearch_tbSearchText')
			textBox.send_keys('Population Change')
			break
		except NoSuchElementException:
			sleep(0.5)
	while True:
		try:
			buttonSearch = driver.find_element_by_id('ctl00_MainContent_ctlSearch_btnSearch')
			response = buttonSearch.click()
			break
		except NoSuchElementException:
			sleep(0.5)
	while True:
		try:
			link = driver.find_element_by_id('ctl00_MainContent_rptSearchResults_ctl04_lbSelectTable')
			link.click()
			break
		except NoSuchElementException:
			sleep(0.5)
	while True:
		try:
			link = driver.find_element_by_id('ctl00_MainContent_ctl02_lblSelectAll')
			link.click()
			break
		except NoSuchElementException:
			sleep(0.5)
	while True:
		try:
			link = driver.find_element_by_id('ctl00_MainContent_ctl04_lblSelectAll')
			link.click()
			break
		except NoSuchElementException:
			sleep(0.5)
	while True:
		try:
			button = driver.find_element_by_id('ctl00_MainContent_btnGo')
			button.click()
			break
		except NoSuchElementException:
			sleep(0.5)
	page = BeautifulSoup(driver.page_source, "lxml")
	pxtable = page.findAll('table', 'pxtable')
	df = pd.read_html(str(pxtable[1]))
	df[0].drop(0, inplace=True)
	df[0].reset_index(drop=True, inplace=True)
	df[0].columns = ['Year', 'PopulationChange', 'PercentPopulationChange', 'NaturalIncrease', 'NetMigration']
	return df[0]

def normalize(x):
    x_norm = []
    xmin = min(x)
    xmax = max(x)
    for i in range(0, len(x)):
        x_norm.append(float(((x[i] - xmin) * 100)/ (xmax - xmin))/100)
    return x_norm

def calculate_regression_goodness_of_fit(ys, y_hat):
    ss_total = 0
    ss_residual = 0
    ss_regression = 0
    y_mean = ys.mean()

    for index in range(len(ys)):
        ss_total += np.square(ys[index] - y_mean)
        ss_residual += np.square(ys[index] - y_hat[index])
        ss_regression += np.square(y_hat[index] - y_mean)

    r_square = ss_regression / ss_total
    rmse = np.sqrt( ss_residual / float(len(ys)) )

    return r_square, rmse

def determine_best_k(vector_in, vector_out):
    lowest_r_square = 100
    best_k = 0
    row, col = vector_in.shape
    for k in range(1, len(vector_out)):
        kNN = KNeighborsClassifier(n_neighbors=k, metric='euclidean', weights='uniform')
        kNN.fit(vector_in, vector_out)
        x = np.reshape(vector_in, (len(vector_in), col)) + 0.001
        y_hat = kNN.predict(x)
        r_square, rmse = calculate_regression_goodness_of_fit(vector_out, y_hat)
        if r_square < lowest_r_square:
            lowest_r_square = r_square
            best_k = k
    return lowest_r_square, best_k
