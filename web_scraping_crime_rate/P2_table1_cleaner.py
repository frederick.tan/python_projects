import pandas as pd
import numpy as np
import calendar
from p2_phantomjs_selenium_util import *
from p2_table2_cleaner import *

offence_mapping = {'dangerous or negligent acts endangering persons': 'EndangeringOthers'
                  	,'abduction, harassment and other related offences against a person': 'Abduction'
                  	 ,'unlawful entry with intent/burglary, break and enter': 'Trespassing'
                  	 ,'theft and related offences': 'Theft'
                  	 ,'fraud, deception and related offences': 'Fraud'
                  	 ,'prohibited and regulated weapons and explosives offences': 'IllegalWeapon'
                  	 ,'property damage and environmental pollution': 'PropertyDamage'
                  	 ,'acts intended to cause injury': 'Injure'
                  	 ,'offences against justice procedures, government security and government operations': 'AgainstGovernment'
                  	 ,'miscellaneous offences': 'Miscellaneous'
                  	 ,'robbery, extortion and related offences': 'Robbery'
                  	 ,'homicide and related offences': 'Homicide'
                  	 ,'illicit drug offences': 'Drugs'
                 	 ,'public order offences': 'PublicDisorder'
                 	 ,'sexual assault and related offences': 'Sexual'
                 	 ,'traffic and vehicle regulatory offences': 'Traffic'
                 	 ,'offences against justice procedures, govt sec and govt ops': 'GovtOffence'
                 	 ,'total offences': 'Total'}

def transform_table1_data(df):
	df.rename(columns={'CRIME':'Offence', 'AGEGRP':'Age', 'GENDER':'Gender', 
                   'ETHNICITY':'Ethnicity', 'YEAR':'Year'}, inplace=True)
	df = df[df.Offence != ' Total offenders']
	df = df[df.Age != '  Not Applicable (This can be used where the offender is an organisation.)']
	df = df[df.Age != '  Not Specified (includes Unknown and Not stated/Inadequately described)']
	df = df[df.Gender != '  Not Applicable']
	df = df[df.Gender != '  Not Stated / Inadequately Described']
	df = df[df.Ethnicity != '  Organisation']
	df = df[df.Ethnicity != '  Not Elsewhere Classified']
	df = df[df.Ethnicity != '  Not Stated / Inadequately Described']
	df.reset_index(inplace=True, drop=True)
	df = apply_inplace(df, 'Year', convert_year_string_to_numeric, 0)
	df = apply_inplace(df, 'Offence', convert_offence_desc_to_short_code, 0)
	age_map, gender_map, crime_map, ethnicity_map = create_maps(df)
	df = transpose_offences_to_columns_t1(crime_map, df)
	df = apply_inplace(df, 'Age', replace_age_with_code, age_map)
	df = apply_inplace(df, 'Gender', replace_gender_with_code, gender_map)
	df = apply_inplace(df, 'Ethnicity', replace_ethnicity_with_code, ethnicity_map)
	keys = transpose_category_to_columns(df.loc[0,'Year'], age_map, gender_map, crime_map, ethnicity_map, df).keys()
	df_new = pd.DataFrame(columns=keys)
	df_pct = pd.DataFrame(columns=keys)
	index = 0
	for year in df.Year.unique():
		df_new.loc[index] = transpose_category_to_columns(year, age_map, gender_map, crime_map, ethnicity_map, df)
		df_pct.loc[index] = transpose_category_to_percentage_columns(year, age_map, gender_map, crime_map, ethnicity_map, df)
		index += 1
	df_new = df_new.sort_values('Year').set_index('Year')
	df_pct = df_pct.sort_values('Year').set_index('Year')
	return df_new

def transpose_offences_to_columns_t1(offence_map, df):
	for offence in offence_map:
		df[offence[1]] = 0
	for i in range(len(df)):
		df.ix[i, df.iloc[i].Offence] = df.ix[i, 'COUNT']
	df.drop('COUNT', axis=1, inplace=True)
	df.drop('Offence', axis=1, inplace=True)
	return df

def convert_year_string_to_numeric(year_str):
	month_list = list(calendar.month_name)
	tokens = year_str.split()
	value = month_list.index(tokens[0])
	value = int(tokens[1]) * 100 + value
	return value
