import pandas as pd
import numpy as np
from p2_phantomjs_selenium_util import *

offence_mapping = {'dangerous or negligent acts endangering persons': 'EndangeringOthers'
                  ,'abduction, harassment and other related offences against a person': 'Abduction'
                  ,'unlawful entry with intent/burglary, break and enter': 'Trespassing'
                  ,'theft and related offences': 'Theft'
                  ,'fraud, deception and related offences': 'Fraud'
                  ,'prohibited and regulated weapons and explosives offences': 'IllegalWeapon'
                  ,'property damage and environmental pollution': 'PropertyDamage'
                  ,'acts intended to cause injury': 'Injure'
                  ,'offences against justice procedures, government security and government operations': 'AgainstGovernment'
                  ,'miscellaneous offences': 'Miscellaneous'
                  ,'robbery, extortion and related offences': 'Robbery'
                  ,'homicide and related offences': 'Homicide'
                  ,'illicit drug offences': 'Drugs'
                  ,'public order offences': 'PublicDisorder'
                  ,'sexual assault and related offences': 'Sexual'
                  ,'traffic and vehicle regulatory offences': 'Traffic'
                  ,'offences against justice procedures, govt sec and govt ops': 'GovtOffence'
                  ,'total offences': 'Total'}

def create_maps(df):
	age_map = map_labels_to_values(np.sort(df.Age.unique().tolist()))
	gender_map = map_labels_to_values(np.sort(df.Gender.unique().tolist()))
	crime_map = map_labels_to_values(np.sort(df.Offence.unique().tolist()))
	ethnicity_map = map_labels_to_values(np.sort(df.Ethnicity.unique().tolist()))
	return age_map, gender_map, crime_map, ethnicity_map

# Thanks to: http://blog.lanzani.nl/2015/apply-in-place/
def apply_inplace(df, column, function, param):
	if param == 0:
		return pd.concat([df.drop(column, axis=1), df[column].apply(function)], axis=1)
	else:
		return pd.concat([df.drop(column, axis=1), df[column].apply(function, args=[param])], axis=1)

def transform_table2_data_raw(df):
	df.drop(['Resolution', 'Flags'], axis=1, inplace=True)
	# Drop the Total Offences as we are analyzing the offences individually
	df = df[df.Offence != 'Total Offences']
	df.reset_index(drop=True, inplace=True)
	df = apply_inplace(df, 'Year', convert_string_year_to_numeric_year, 0)
	df = apply_inplace(df, 'Offence', convert_offence_desc_to_short_code, 0)
	age_map, gender_map, crime_map, ethnicity_map = create_maps(df)
	df = transpose_offences_to_columns(crime_map, df)
	return df
	
def transform_table2_data(df):
	df.drop(['Resolution', 'Flags'], axis=1, inplace=True)
	# Drop the Total Offences as we are analyzing the offences individually
	df = df[df.Offence != 'Total Offences']
	df.reset_index(drop=True, inplace=True)
	df = apply_inplace(df, 'Year', convert_string_year_to_numeric_year, 0)
	df = apply_inplace(df, 'Offence', convert_offence_desc_to_short_code, 0)
	age_map, gender_map, crime_map, ethnicity_map = create_maps(df)
	df = apply_inplace(df, 'Age', replace_age_with_code, age_map)
	df = apply_inplace(df, 'Gender', replace_gender_with_code, gender_map)
	df = apply_inplace(df, 'Ethnicity', replace_ethnicity_with_code, ethnicity_map)
	df = transpose_offences_to_columns(crime_map, df)
	keys = transpose_category_to_columns(1995, age_map, gender_map, crime_map, ethnicity_map, df).keys()
	df_new = pd.DataFrame(columns=keys)
	df_pct = pd.DataFrame(columns=keys)
	index = 0
	for year in df.Year.unique():
		df_new.loc[index] = transpose_category_to_columns(year, age_map, gender_map, crime_map, ethnicity_map, df)
		df_pct.loc[index] = transpose_category_to_percentage_columns(year, age_map, gender_map, crime_map, ethnicity_map, df)
		index += 1
	df_new = df_new.sort_values('Year').set_index('Year')
	df_pct = df_pct.sort_values('Year').set_index('Year')

	return df_pct

def convert_string_year_to_numeric_year(year_str):
    tokens = year_str.split()
    return int(tokens[3])

def convert_offence_desc_to_short_code(offence):
    return offence_mapping[offence.lower()]

def replace_name_with_code(name, code_map):
    for entry in code_map:
        if entry[1] == name:
            return entry[0]
    return len(code_map)

def replace_age_with_code(name, age_map):
    return replace_name_with_code(name, age_map)

def replace_gender_with_code(name, gender_map):
    return replace_name_with_code(name, gender_map)

def replace_ethnicity_with_code(name, ethnicity_map):
    return replace_name_with_code(name, ethnicity_map)

def transpose_category_to_columns(year, age_map, gender_map, crime_map, ethnicity_map, df):
    data = pd.Series()
    temp = df[df.Year == year].groupby('Age').count()
    for i in range(temp['Gender'].count()):
        data[age_map[i][1]] = temp['Gender'][i]
    temp = df[df.Year == year].groupby('Gender').count()
    for i in range(temp['Ethnicity'].count()):
        data[gender_map[i][1]] = temp['Ethnicity'][i]
    temp = df[df.Year == year].groupby('Ethnicity').count()
    for i in range(temp['Age'].count()):
        data[ethnicity_map[i][1]] = temp['Age'][i]
    for header in list(df.columns[4:]):
        data[header] = sum(df[df.Year == year][header])
    data['Year'] = year
    return data

def transpose_category_to_percentage_columns(year, age_map, gender_map, crime_map, ethnicity_map, df):
    data = pd.Series()
    temp = df[df.Year == year].groupby('Age').count()
    for i in range(temp['Gender'].count()):
        data[age_map[i][1]] = (temp['Gender'][i]*100)/temp['Gender'].sum()
    temp = df[df.Year == year].groupby('Gender').count()
    for i in range(temp['Ethnicity'].count()):
        data[gender_map[i][1]] = (temp['Ethnicity'][i]*100)/temp['Ethnicity'].sum()
    temp = df[df.Year == year].groupby('Ethnicity').count()
    for i in range(temp['Age'].count()):
        data[ethnicity_map[i][1]] = (temp['Age'][i]*100)/temp['Age'].sum()
    for header in list(df.columns[4:]):
        data[header] = float((sum(df[df.Year == year][header])*1000)/sum(sum(df[df.columns[4:]][df.Year == year].values)))
    data['Year'] = year
    return data

def transpose_offences_to_columns(offence_map, df):
	for crime in offence_map:
		df[crime[1]] = 0
	for i in range(len(df)):
		df.ix[i, df.iloc[i].Offence] = df.ix[i, 'Value']
	df.drop('Value', axis=1, inplace=True)
	df.drop('Offence', axis=1, inplace=True)
	return df
