import datetime
import numpy as np
import pandas as pd
import re
import requests
from bs4 import BeautifulSoup
from nltk.tokenize import RegexpTokenizer
from re import sub
def get_color_table():
	# Prepare color_table
	response = requests.get('https://simple.wikipedia.org/wiki/List_of_colors')
	page = response.content
	colors_wiki = BeautifulSoup(page, 'lxml')
	color_wiki_table = colors_wiki.find_all('table')[0]
	color_table_trs = color_wiki_table.find_all('tr')
	color_table = list()
	for tr in color_table_trs:
	    tds = tr.find_all('td')
	    if len(tds) > 0:
	        color_table.append(str(tds[0].text.lower()))
	color_table.append('quartz')
	color_table.append('graphite')
	color_table.append('carbon')
	color_table.append('grey')
	color_table.append('rosegold')
	color_table.append('midnightblack')
	color_table.append('midnightpink')
	color_table.append('spacegray')
	del colors_wiki, color_wiki_table, color_table_trs
	return color_table

def split_string(string, separator, maxSplit):
    #stringToSplit = string
    return string.split(separator, maxSplit)

def get_os_type(osString):
	if osString == 'unknown':
		return osString
	return split_string(osString, ' ', 2)[0]

def get_os_version(osString):
    version = re.sub(' ', '', re.sub('[a-zA-Z()]*','',osString))
    if version == '':
        return 0.0
    elif len(str(version).split('.')) > 2:
        # We don't care about the other minor versions, I think this is acceptable
        split_version = str(version).split('.')
        return float(str(split_version[0] + '.' + split_version[1]))
    else:
        return float(version)

def get_screen_resolution(resolution, axis):
    index = 0
    #print 'res:', resolution
    if resolution == 'unknown' or resolution == '':
    	return 0
    if axis == 'x':
        index = 0
    else:
        index = 1
    #print resolution
    new_string = split_string(resolution, 'x', 2)
    if len(new_string) < 2:
    	return 0
    return new_string[index]

def get_color_from_description(description, color_table):
	tokenizer = RegexpTokenizer('\w+')
	tokenized = tokenizer.tokenize(description)
	color_list = [token for token in tokenized if token.lower() in color_table]
	if len(color_list) > 0:
		return color_list[0].lower()
	else:
		return 'unknown'

def get_manufacturer(x):
    return x.split()[0]

def convert_currency_to_float(currencyString):
    currency = float(sub(r'[^\d\-.]', '', currencyString))
    return currency

def convert_hist_id_to_numeric(x):
    return float(split_string(x, '_',3)[2])

def decipher_color_in_details(detail):
    detail = detail.lower().replace('wh..', ' white')
    detail = detail.lower().replace('b..', ' black')
    detail = detail.lower().replace('g..', ' gold')
    detail = detail.lower().replace('s..', ' silver')
    detail = detail.lower().replace('sil..', ' silver')
    detail = detail.lower().replace('space_gray', ' spacegray')
    detail = detail.lower().replace('rose gold', ' rosegold')
    detail = detail.lower().replace('-wh..', ' white')
    detail = detail.lower().replace('-bl..', ' black')
    detail = detail.lower().replace('midnight b...', ' midnightblack')
    detail = detail.lower().replace('midnight p...', ' midnightpink')
    return detail

def divide_user_rating(x):
	# 0-4, 5-7, 8, 9-10
	new_category = 0
	if x >= 9:
		new_category = 4
	elif x == 8:
		new_category = 3
	elif x >= 5:
		new_category = 2
	else:
		new_category = 1
	return float(new_category)

def calculate_date_diff_in_months(date1_str, date2_str):
    #print date2_str
    #if date2_str.isnull():
    #z    return 0
    date1 = datetime.datetime.strptime(date1_str, "%Y-%m-%d").date()
    date2 = datetime.datetime.strptime(date2_str, "%Y-%m-%d").date()
    y1 = date1.year
    m1 = date1.month
    y2 = date2.year
    m2 = date2.month
    return ((y2 - y1) * 12) + (m2 - m1)

def add_historical_price(df, data_hist):
    simple_date_price_pct_df = pd.DataFrame(columns=['id', 'month', 'price_change'])
    index_sdppdf = 0
    data = pd.DataFrame()
    data = df.reset_index(drop=True)
    for index, row in data.iterrows():
        hist = data_hist[data_hist.id == row.id][data_hist.store_name == row.store_name][data_hist.color == row.color]
        if len(hist) == 0:
            continue
        hist.sort_values(by='date', ascending=True, inplace=True)
        hist = hist.reset_index(drop=True)
        last_index = len(hist) - 1
        data.loc[index, 'first_rel'] = hist.date[0]
        data.loc[index, 'last_rel'] = hist.date[last_index]
        data.loc[index, 'first_price'] = hist.hist_price[0]
        data.loc[index, 'last_price'] = hist.hist_price[last_index]
        data.loc[index, 'avg_price'] = hist.hist_price.mean()
        data.loc[index, 'min_price'] = hist.hist_price.min()
        data.loc[index, 'max_price'] = hist.hist_price.max()
        data.loc[index, 'min_date'] = hist.loc[hist.hist_price.idxmin(), 'date']
        data.loc[index, 'max_date'] = hist.loc[hist.hist_price.idxmax(), 'date']
        max_price_drop_date = ''
        max_price_drop_pct = 0
        prev_price= -1
        cur_price = -1
        pct_change = 0
        for ix, row in hist.iterrows():
            postfix = str(ix)
            if prev_price == -1:
                prev_price = row.hist_price
                max_price_drop_date = row.date # Let's init this to the first date
            else:
                cur_price = row.hist_price
                pct_change = 100*float(prev_price - cur_price)/prev_price
                prev_price = cur_price
                #print pct_change, prev_price, cur_price
                if pct_change > max_price_drop_pct:
                    max_price_drop_pct = pct_change
                    max_price_drop_date = row.date
            #if ix == len(hist) - 1:
            #    postfix = 'last'
            column_name_price = 'hist_price_' + postfix
            column_name_date = 'hist_date_' + postfix
            column_name_month = 'hist_month_' + postfix
            column_name_pct = 'hist_pct_' + postfix
            data.loc[index, column_name_price] = row.hist_price
            data.loc[index, column_name_date] = row.date
            month = float(datetime.datetime.strptime(row.date, "%Y-%m-%d").date().month)
            data.loc[index, column_name_month] = month
            if ix == 0:
                data.loc[index, 'hist_month_0'] = month
            data.loc[index, column_name_pct] = pct_change
            simple_date_price_pct_df.loc[index_sdppdf, 'id'] = row.id
            simple_date_price_pct_df.loc[index_sdppdf, 'month'] = month
            simple_date_price_pct_df.loc[index_sdppdf, 'price_change'] = pct_change
            #print index, column_name_pct, pct_change
            index_sdppdf += 1
        data.loc[index, 'max_price_drop_month'] = float(datetime.datetime.strptime(max_price_drop_date, "%Y-%m-%d").date().month)
        data.loc[index, 'max_price_drop_pct'] = max_price_drop_pct
        try:
            months_to_max_price_drop = data.loc[index, 'max_price_drop_month'] - data.loc[index, 'hist_month_0']
        except KeyError as e:
            months_to_max_price_drop = 12

        try:
            #months_to_first_price_drop = data.loc[index, 'hist_month_1'] - data.loc[index, 'hist_month_0']
            if type(data.loc[index, 'hist_date_1']) == unicode:
                months_to_first_price_drop = calculate_date_diff_in_months(data.loc[index, 'hist_date_0'], data.loc[index, 'hist_date_1'])
            else:
                months_to_first_price_drop = 27
        except KeyError as e:
            months_to_first_price_drop = 27
        if months_to_max_price_drop < 0:
            months_to_max_price_drop = 12 + months_to_max_price_drop
        if months_to_first_price_drop < 0:
            months_to_first_price_drop = 12 + months_to_first_price_drop
        data.loc[index, 'months_to_max_price_drop'] = months_to_max_price_drop
        data.loc[index, 'months_to_first_price_drop'] = months_to_first_price_drop
    return data, simple_date_price_pct_df

def add_historical_price_merge_store_color(df, data_hist):
	simple_date_price_pct_df = pd.DataFrame(columns=['id', 'month', 'price_change'])
	data = pd.DataFrame()
	data = df.sort_values(by='id').reset_index(drop=True)
	ids = data.id.unique()
	data_table = data.pivot_table(index=['id'])
	history_table = data_hist.drop(['hist_id'], axis=1).pivot_table(index=['id', 'date', 'color'])
	simple_index = 0
	for id in ids:
		specific_id = [index for index in history_table.index if index[0] == id]
		#print specific_id[0]
		if len(specific_id) < 3:
			continue
		specific_hist_table = history_table[history_table.index.get_level_values('id') == specific_id[0][0]]
		dates = specific_hist_table.index.get_level_values('date')
		counter = 0
		dates = sorted(dates)
		largest_decrease_date = ''
		largest_decrease_pct = 0
		prev_price= -1
		cur_price = -1
		pct_change = 0
		if len(dates) <= 1:
			continue
		#print dates
		for count, date in zip(range(len(dates)), dates):
			#print date
			postfix = str(count)
			if prev_price == -1:
				prev_price = specific_hist_table.loc[[id, date], 'hist_price'][count]
			else:
				cur_price = specific_hist_table.loc[[id, date], 'hist_price'][count]
				pct_change = 100*float(prev_price - cur_price)/prev_price
				prev_price = cur_price
				if pct_change > largest_decrease_pct:
					largest_decrease_pct = pct_change
					largest_decrease_date = date
				simple_date_price_pct_df.loc[simple_index, 'id'] = id
				simple_date_price_pct_df.loc[simple_index, 'month'] = get_month(date)
				simple_date_price_pct_df.loc[simple_index, 'price_change'] = pct_change
				simple_index += 1
			if count == 0:
				data_table.loc[id, 'first_rel'] = date
				data_table.loc[id, 'first_price'] = specific_hist_table.loc[[id, date], 'hist_price'][count]
			elif count == len(dates) - 1:
				data_table.loc[id, 'last_rel'] = date
				data_table.loc[id, 'last_price'] = specific_hist_table.loc[[id, date], 'hist_price'][count]
				data_table.loc[id, 'hist_date_last'] = date
			column_name_price = 'hist_price_' + postfix
			column_name_date = 'hist_date_' + postfix
			data_table.loc[id, column_name_price] = specific_hist_table.loc[[id, date], 'hist_price'][count]
			data_table.loc[id, column_name_date] = date
		data_table.loc[id, 'months_first_drop'] = calculate_date_diff_in_months(data_table.loc[id, 'first_rel'], data_table.loc[id, 'hist_date_1'])
		data_table.loc[id, 'month_first_drop'] = get_month(data_table.loc[id, 'hist_date_1'])
		data_table.loc[id, 'avg_price'] = specific_hist_table.hist_price.mean()
		data_table.loc[id, 'min_price'] = specific_hist_table.hist_price.min()
		data_table.loc[id, 'max_price'] = specific_hist_table.hist_price.max()
		data_table.loc[id, 'min_date'] = str(specific_hist_table.hist_price.idxmin()[1])
		data_table.loc[id, 'max_date'] = str(specific_hist_table.hist_price.idxmax()[1])
		data_table.loc[id, 'max_price_drop_date'] = largest_decrease_date
		data_table.loc[id, 'max_price_drop_pct'] = largest_decrease_pct
		simple_date_price_pct_df = simple_date_price_pct_df.astype(np.number)
	return data_table, simple_date_price_pct_df

def add_historical_price_merge_store(df, data_hist):
	simple_date_price_pct_df = pd.DataFrame(columns=['id', 'month', 'price_change'])
	data = pd.DataFrame()
	data = df.sort_values(by='id').reset_index(drop=True)
	ids = data.id.unique()
	data_table = data.pivot_table(index=['id'])
	history_table = data_hist.drop(['hist_id'], axis=1).pivot_table(index=['id', 'date'])
	simple_index = 0
	for id in ids:
		specific_hist_table = history_table[history_table.index.get_level_values('id') == id]
		dates = specific_hist_table.index.get_level_values('date')
		counter = 0
		dates = sorted(dates)
		largest_decrease_date = ''
		largest_decrease_pct = 0
		prev_price= -1
		cur_price = -1
		pct_change = 0
		if len(dates) <= 1:
			continue
		#print dates
		for count, date in zip(range(len(dates)), dates):
			#print date
			postfix = str(count)
			if prev_price == -1:
				prev_price = specific_hist_table.loc[[id, date], 'hist_price'][count]
			else:
				cur_price = specific_hist_table.loc[[id, date], 'hist_price'][count]
				pct_change = 100*float(prev_price - cur_price)/prev_price
				prev_price = cur_price
				if pct_change > largest_decrease_pct:
					largest_decrease_pct = pct_change
					largest_decrease_date = date
				simple_date_price_pct_df.loc[simple_index, 'id'] = id
				simple_date_price_pct_df.loc[simple_index, 'month'] = get_month(date)
				simple_date_price_pct_df.loc[simple_index, 'price_change'] = pct_change
				simple_index += 1
			if count == 0:
				data_table.loc[id, 'first_rel'] = date
				data_table.loc[id, 'first_price'] = specific_hist_table.loc[[id, date], 'hist_price'][count]
			elif count == len(dates) - 1:
				data_table.loc[id, 'last_rel'] = date
				data_table.loc[id, 'last_price'] = specific_hist_table.loc[[id, date], 'hist_price'][count]
				data_table.loc[id, 'hist_date_last'] = date
			column_name_price = 'hist_price_' + postfix
			column_name_date = 'hist_date_' + postfix
			data_table.loc[id, column_name_price] = specific_hist_table.loc[[id, date], 'hist_price'][count]
			data_table.loc[id, column_name_date] = date
		data_table.loc[id, 'months_first_drop'] = calculate_date_diff_in_months(data_table.loc[id, 'first_rel'], data_table.loc[id, 'hist_date_1'])
		data_table.loc[id, 'month_first_drop'] = get_month(data_table.loc[id, 'hist_date_1'])
		data_table.loc[id, 'avg_price'] = specific_hist_table.hist_price.mean()
		data_table.loc[id, 'min_price'] = specific_hist_table.hist_price.min()
		data_table.loc[id, 'max_price'] = specific_hist_table.hist_price.max()
		data_table.loc[id, 'min_date'] = str(specific_hist_table.hist_price.idxmin()[1])
		data_table.loc[id, 'max_date'] = str(specific_hist_table.hist_price.idxmax()[1])
		data_table.loc[id, 'max_price_drop_date'] = largest_decrease_date
		data_table.loc[id, 'max_price_drop_pct'] = largest_decrease_pct
		simple_date_price_pct_df = simple_date_price_pct_df.astype(np.number)
	return data_table, simple_date_price_pct_df

# Calculate the price decrease in percentage every month since first released
def calculate_monthly_price_decrease(df):
    for index, row in df.iterrows():
        first_date = row.first_rel
        first_price = row.first_price
        unused_price = first_price
        period = 0
        first_price_drop = -1
        for i in range(1, 27):
            his_date = 'hist_date_' + str(i)
            his_price = 'hist_price_' + str(i)
            if type(row[his_date]) == unicode:
                date_diff = calculate_date_diff_in_months(first_date, row[his_date])
                new_period = int(date_diff / 6)
               	pct_change = 100*float(first_price - unused_price)/first_price
               	if first_price_drop == -1:
               		df.loc[index, 'first_price_drop'] = pct_change
               		df.loc[index, 'first_price_drop_months'] = date_diff
                if new_period > period:
                    unused_price = row[his_price]
                    for m in range(period, new_period + 1):
                        col_price_drop = 'pct_price_drop_' + str(m)
                        df.loc[index, col_price_drop] = pct_change
                    period = new_period
                else:
                    unused_price = row[his_price]
            else:
                break
    return df

def convert_to_category(df, column):
    column_values = df[column].unique()
    for new_column in column_values:
        df[new_column] = float(0)
    for index, row in df.iterrows():
        df.loc[index, row[column]] = float(1)
    return df

def get_month(date_string):
    return datetime.datetime.strptime(date_string, "%Y-%m-%d").date().month

def is_recent(date_string):
    month = float(datetime.datetime.strptime(date_string, "%Y-%m-%d").date().month)
    year = float(datetime.datetime.strptime(date_string, "%Y-%m-%d").date().year)
    if year == 2016 and month >= 10:
        return True
    return False

def fix_hist_data(df):
    for index, row in df.iterrows():
        if row.hist_pct_1 == 0.0:
            df.loc[index, 'hist_pct_1'] = row.hist_pct_2
            df.loc[index, 'months_to_first_price_drop'] = df.loc[index, 'hist_month_2'] - df.loc[index, 'hist_month_0']
    return df

def convert_to_float_with_unknown(x):
	float_x = 0
	try:
		float_x = float(x)
	except:
		float_x = float(0)
	return float_x

def load_and_clean_current_data_excel(filename_xls):
	df = pd.read_excel(filename_xls)
	return load_and_clean_current_data(df)

def load_and_clean_current_data(df):
	color_table = get_color_table()
	#df = pd.read_excel(filename_xls)
	df.rename(columns={'screen_solution':'screen_resolution'}, inplace=True)
	#remove null data
	#df = df.where(~(df['operating_system']=='') |
    #              ~(df['screen_size']=='')|
    #              ~(df['screen_resolution']=='')).dropna(axis=0)
	df = df.fillna('unknown')
	df.loc[:,'price'] = df.loc[:,'price'].str.replace(',','')
	df.loc[:,'price_lowest'] = df.loc[:,'price_lowest'].str.replace('$','')
	df.loc[:,'price_lowest'] = df.loc[:,'price_lowest'].str.replace(',','')
	df.loc[:,'screen_size'] = df.loc[:,'screen_size'].str.replace('inches','')
	df.loc[:,'screen_resolution'] = df.loc[:,'screen_resolution'].str.replace('pixels','')
	df.loc[:,'user_rating'] = df.loc[:,'user_rating'].str.replace('/10','')
	df['os'] = df.operating_system.apply(get_os_type)
	df['os_v'] = df.operating_system.apply(get_os_version)
	df['srw'] = df.screen_resolution.apply(lambda x: get_screen_resolution(x, 'x')).astype(np.number)
	df['srh'] = df.screen_resolution.apply(lambda x: get_screen_resolution(x, 'h')).astype(np.number)
	df['price'] = df.price.astype(np.number)
	df['price_lowest'] = df.price_lowest.apply(convert_to_float_with_unknown)
	df['screen_size'] = df.screen_size.apply(convert_to_float_with_unknown)
	df['user_rating'] = df.user_rating.apply(convert_to_float_with_unknown)
	df['product_name'] = df['product_name'].apply(decipher_color_in_details)
	df['color'] = df.product_name.apply(lambda x: get_color_from_description(x, color_table))
	df['user_rating_cat'] = df.user_rating.apply(divide_user_rating)
	df['manufacturer'] = df.name.apply(get_manufacturer)
	df.drop(['operating_system', 'screen_resolution', 'product_name'], axis=1, inplace=True)
	return df

def load_and_clean_historical_data(filename_xls):
	color_table = get_color_table()
	df = pd.read_excel(filename_xls)
	df['hist_description'] = df.hist_description.apply(decipher_color_in_details)
	df['hist_price'] = df.hist_price.apply(convert_currency_to_float)
	df['hist_id'] = df.hist_id.apply(convert_hist_id_to_numeric)
	df['color'] = df.hist_description.apply(lambda x: get_color_from_description(x, color_table))
	df['id'] = df.id.astype(np.number)
	return df
