# Import the python libraries
import datetime
import gmplot
import googlemaps
import numpy as np
import os.path
import pandas as pd
from IPython.core.display import display, HTML
from os import listdir
from os.path import isfile, join
from bs4 import BeautifulSoup

data_dir = '.\datasets\\'
def configure_data_dir(new_data_dir):
	data_dir = new_data_dir

def machine_data_to_dataframe(collated_machine_data_filename):
	data_path = data_dir + collated_machine_data_filename
	df = pd.DataFrame()
	if os.path.exists(data_path):
		df = pd.read_csv(data_path, index_col=0)
	else:
		files = [f for f in listdir('.\MachineData') if isfile(join('.\MachineData', f))]
		columns = pd.Series(['id', 'event_timestamp', 'course_over_ground', 'machine_id',
							 'vehicle_weight_type', 'speed_gps_kph', 'latitude', 'longitude'],
							index=['id', 'event_timestamp', 'course_over_ground', 'machine_id',
								   'vehicle_weight_type', 'speed_gps_kph', 'latitude', 'longitude'])
		df = pd.DataFrame(columns = columns)
		for fileName in files:
			path ='.\MachineData\\' + fileName
			df = pd.concat([df, pd.read_csv(path, sep=';')])
	df['hour_raw'] = df.time.apply(convert_time_string_to_raw_hour)
	df['hour'] = df.time.apply(convert_time_string_to_hour)
	df = df.sort_values(by='id').reset_index(drop=True)
	return df

# Find the road segment in which a vehicle is travelling at a given time
# This is a simple case of finding a poing in a rectangle, that is,
# The rectangle coordinates are defined by:
#	road segment Latitude(n), Longitude(n), Latitude(n+1), Longitude(n+1)
# And the point is given by the vehicle's given Latitude and Longitude
def find_road_segment(row):
	longitude = row.longitude
	latitude = row.latitude
	for row_index, segment in road_segment_df.iterrows():
		coords_text = segment.road_segment_wkt
		coords_text = coords_text.replace(')',',')
		coords_text = re.findall(r'-?\d+(?:\.\d*)?', coords_text.rpartition(',')[0])
		coords_text = [float(i) for i in coords_text]
		coords = zip(*[iter(coords_text)] * 2)
		# Our objective is to find if the point is between a segment
		index = 0
		for index in range(len(coords) - 1):
			bw_longitude = False
			bw_latitude = False
			long_1, lat_1 = coords[index]
			long_2, lat_2 = coords[index + 1]
			#print long_1, lat_1, long_2, lat_2, longitude, latitude
			if (longitude == long_1) or (longitude == long_2):
				bw_longitude = True
			elif (long_1 > long_2):
				if (longitude < long_1) and (longitude > long_2):
					bw_longitude = True
			else:
				if (longitude < long_2) and (longitude > long_1):
					bw_longitude = True

			if (latitude == lat_1) or (latitude == lat_2):
				bw_latitude = True
			elif (lat_1 > lat_2):
				if (latitude < lat_1) and (latitude > lat_2):
					bw_latitude = True
			else:
				if (latitude < lat_2) and (latitude > lat_1):
					bw_latitude = True
			if bw_longitude == True and bw_latitude == True:
				return segment.id

def weather_data_to_dataframe(weather_data_filename):
	data_path = data_dir + weather_data_filename
	df = pd.DataFrame()
	if os.path.exists(data_path):
		df = pd.read_csv(data_path, index_col=0)
	else:
		df = pd.DataFrame()
		for day in range(15,21):
			filename = data_dir + '.\september' + str(day) + '_weather.csv'
			temp = pd.read_csv(filename)
			temp['date'] = '2015-11-' + str(day)
			df = pd.concat([df, temp])
		df.to_csv(data_path)
	df['hour'] = df.TimeNZST.apply(convert_time_string_w_am_pm)
	df = df[['date', 'hour', 'VisibilityKm', 'Conditions', 'WindDirDegrees', 'Wind SpeedKm/h']]
	#df = df.drop(df.index[0:22]).reset_index(drop=True)
	df = df.reset_index(drop=True)
	return df

def segmented_data_by_destination_to_dataframe(df, data_machines_filename):
	bus_list = list()
	data_path = data_dir + data_machines_filename
	if os.path.exists(data_path):
		df_machines = pd.read_csv(data_path, index_col=0)
		bus_list = list(df_machines[df_machines.type == 'POSSIBLY_BUS'].machine_id.unique())
	else:
		df = df.sort_values(by='id')
		machines = list(df.machine_id.astype(int).unique())
		columns=list({'machine_id', 'start_segment', 'start_time', 'date', 'end_time', 'end_segment', 'speed_gps_kph',
					  'segments', 'start_latitude', 'start_longitude', 'end_latitude', 'end_longitude', 'type'})
		df_machines = pd.DataFrame(columns = columns)
		index = 0
		for machine in machines:
			temp_df_machines = df[df.machine_id == machine]
			record_complete = 0
			data = pd.Series()
			count = 0
			segments = []
			zero_count = 0
			is_bus = False
			for row in temp_df_machines.iterrows():
				while True:
					if record_complete == 0:
						data['date'] = row[1]['date']
						data['end_date'] = row[1]['date']
						data['end_time'] = row[1]['hour_raw']
						data['start_time'] = row[1]['hour_raw']
						data['machine_id'] = machine
						data['end_segment'] = row[1]['road_segment_id']
						data['end_latitude'] = row[1]['latitude']
						data['end_longitude'] = row[1]['longitude']
						data['start_segment'] = row[1]['road_segment_id']
						data['start_latitude'] = row[1]['latitude']
						data['start_longitude'] = row[1]['longitude']
						if row[1]['speed_gps_kph'] == 0:
							zero_count = zero_count + 1
						speed_gps_kph = row[1]['speed_gps_kph']
						record_complete = 1
						count = 1
						del segments[:]
						if np.isnan(row[1]['road_segment_id']) == False:
							segments.append(row[1]['road_segment_id'])
						break
					else:
						if data['end_date'] != row[1]['date']:
							hour_diff = row[1]['hour_raw'] + 24
							if (hour_diff - data['end_time'])> 0.5:
								record_complete = 0
							else:
								data['end_date'] = row[1]['date']
								data['end_time'] = row[1]['hour_raw']
						elif (row[1]['hour_raw'] - data['end_time']) > 0.5:
							record_complete = 0
						else:
							if np.isnan(data['start_segment']) == True:
								data['start_segment'] = row[1]['road_segment_id']
								data['start_latitude'] = row[1]['latitude']
								data['start_longitude'] = row[1]['longitude']
							data['end_time'] = row[1]['hour_raw']
							data['end_date'] = row[1]['date']
							if row[1]['speed_gps_kph'] == 0:
								zero_count = zero_count + 1
							if np.isnan(row[1]['road_segment_id']) == False:
								data['end_segment'] = row[1]['road_segment_id']
								data['end_latitude'] = row[1]['latitude']
								data['end_longitude'] = row[1]['longitude']
								segments.append(row[1]['road_segment_id'])
							speed_gps_kph += row[1]['speed_gps_kph']
							count = count + 1
							break
						if record_complete == 0:
							speed_gps_kph /= count
							data['speed_gps_kph'] = speed_gps_kph
							data['segments'] = list(set(segments))
							if zero_count > 7 and row[1]['vwt'] == 'HEAVY':
								data['type'] = 'POSSIBLY_BUS'
								bus_list.append(machine)
							else:
								data['type'] = row[1]['vwt']
							df_machines.loc[index] = data
							count = 0
							zero_count = 0
							index = index + 1

		df_machines = df_machines[['date', 'machine_id', 'type', 'start_time', 'end_time', 'start_segment', 'end_segment',
							   'speed_gps_kph', 'segments']]
		df_machines.to_csv(data_path)
		bus_list = list(set(bus_list))
	df.loc[df['machine_id'].isin(bus_list), 'vwt'] = 'POSSIBLY_BUS'
	return df, df_machines

def convert_time_string_to_raw_hour(time_string):
	hour = datetime.datetime.strptime(time_string, "%H:%M:%S").time().hour
	minute = datetime.datetime.strptime(time_string, "%H:%M:%S").time().minute
	hour = hour + float(minute/60.0)
	return hour

def print_number_of_vehicles_per_day():
	machines = {}
	print 'Number of vehicles per day:'
	for date in dates_covered:
		machines[date] = df[df.date == date].machine_id.unique()
		print date, len(machines[date])

def print_number_of_road_segments_used_per_day():
	used_road_segments = {}
	print 'Number of road segments used per day:'
	for date in dates_covered:
		used_road_segments[date] = df[df.date == date].road_segment_id.unique()
		print date, len(used_road_segments[date])

def convert_time_string_to_hour(time_string):
	hour = datetime.datetime.strptime(time_string, "%H:%M:%S").time().hour
	minute = datetime.datetime.strptime(time_string, "%H:%M:%S").time().minute
	if minute >= 30:
		hour += 0.5
	else:
		hour += 0.0
	return hour

def convert_time_string_w_am_pm(time_string):
    hour = datetime.datetime.strptime(time_string, "%I:%M %p").time().hour
    minute = datetime.datetime.strptime(time_string, "%I:%M %p").time().minute
    if minute >= 30:
        hour += 0.5
    else:
        hour += 0.0
    return hour

def merge_traffic_df_and_weather_df(traffic_df_var, weather_df_var, merged_df, time_shift, cv_file):
	data_path = data_dir + cv_file
	columns = list(['HEAVY', 'LIGHT', 'POSSIBLY_BUS',  'speed_gps_kph', 'date', 'hour', 'segment'])
	weather_index = list(weather_df_var.drop(['date', 'hour'], axis=1).columns)
	columns = columns + weather_index
	temp = pd.DataFrame(columns=columns)
	if os.path.exists(data_path):
		temp = pd.read_csv(data_path, index_col=0)
	else:
		dates = list(traffic_df_var.date.unique())
		weather_df_var.loc[:,'hour'] = weather_df_var['hour'] - time_shift
		weather_df_var.loc[weather_df_var['date'] == '2015-11-16'].loc[weather_df_var['hour'] < 0.0, 'date'] = '2015-11-15'
		weather_df_var.loc[weather_df_var['date'] == '2015-11-17'].loc[weather_df_var['hour'] < 0.0, 'date'] = '2015-11-16'
		weather_df_var.loc[weather_df_var['date'] == '2015-11-18'].loc[weather_df_var['hour'] < 0.0, 'date'] = '2015-11-17'
		weather_df_var.loc[weather_df_var['date'] == '2015-11-19'].loc[weather_df_var['hour'] < 0.0, 'date'] = '2015-11-18'
		weather_df_var.loc[weather_df_var['date'] == '2015-11-20'].loc[weather_df_var['hour'] < 0.0, 'date'] = '2015-11-19'
		weather_df_var.loc[weather_df_var['hour'] == -0.5, 'hour'] = 23.5
		weather_df_var.loc[weather_df_var['hour'] == -1.0, 'hour'] = 23.0

		for date in dates:
			hours = list(traffic_df_var[traffic_df_var.date == date].hour.unique())
			level1_df = merged_df.xs(date)
			for hour in hours:
				level2_df = level1_df.xs(hour)
				segments = list(traffic_df_var[traffic_df_var.date == date][traffic_df_var.hour == hour].road_segment_id.dropna().unique())
				weather = weather_df_var[weather_df_var.date==date][weather_df_var.hour == hour].reset_index(drop=True).loc[0].drop(['date', 'hour'])
				for segment in segments:
					row = level2_df.xs(segment)
					row = row.append(pd.Series([date, hour, segment], index=['date', 'hour', 'segment']))
					row = row.append(weather)
					temp.loc[len(temp)] = list(row)
		temp = temp[['date', 'hour', 'segment', 'HEAVY', 'LIGHT', 'POSSIBLY_BUS',
				 	 'speed_gps_kph', 'VisibilityKm', 'Conditions',  'WindDirDegrees',  'Wind SpeedKm/h']]
		temp['total'] = temp['HEAVY'] + temp['LIGHT'] + temp['POSSIBLY_BUS']
		temp.to_csv(data_path);
	return temp

def get_correlation_per_segment(source_df):
	segments = list(source_df.segment.unique())
	dummy_df = pd.DataFrame()
	max_corrs_df = pd.DataFrame(columns = ['segment', 'key', 'value'])
	for segment in segments:
		dummy_df = source_df[source_df.segment == segment]
		dummy_df = dummy_df.corr().filter(dummy_df[[6]]).drop(dummy_df[[6]])
		col_name = 'speed_gps_kph_' + str(segment)
		dummy_df.columns = [col_name]
		key = dummy_df.idxmax()
		value = dummy_df.max()
		max_corrs_df.loc[len(max_corrs_df)] = list(pd.Series([segment, key[0], value[0]], index=['segment', 'key', 'value']))
	return max_corrs_df

def find_vehicles_using_road_segment(df, target_segment):
    new_df = pd.DataFrame(columns = df.columns)
    for index, row in df.iterrows():
        for segment in row.segments:
            new_df.loc[len(new_df)] = row
    return new_df

def get_and_prepare_data_for_road_segment(traffic_weather_df, segment):
    df = traffic_weather_df[traffic_weather_df.segment == 606162069]
    df = df[['speed_gps_kph', 'HEAVY', 'LIGHT', 'POSSIBLY_BUS', 'WindDirDegrees', 'condition_code', 'date_code']]
    df['condition_code'] = df.condition_code.astype(float)
    df['date_code'] = df.date_code.astype(float)
    df.reset_index(drop=True, inplace=True)
    return df

def display_corrs(segment_per_df, traffic_weather_df, vehicle_count_th):
    corrs_segments = segment_per_df[segment_per_df.value > 0.5].sort_values(by='value', ascending=False).segment
    index = 0
    print 'index', 'segment', 'segment_usage_count', 'total_vehicles'
    for segment in corrs_segments:
        segment_used_count = len(traffic_weather_df[traffic_weather_df.segment == segment])
        total_vehicles = traffic_weather_df[traffic_weather_df.segment == segment].total.sum()
        if total_vehicles > vehicle_count_th:
            key = segment_per_df[segment_per_df.segment == segment].key.values
            print index, key[0], segment, segment_used_count, total_vehicles
        index = index + 1

def get_coordinates_of_road_segment(segment_id):
    for row_index, segment in road_segment_df.iterrows():
        if segment.id == segment_id:
            coords_text = segment.road_segment_wkt
            coords_text = coords_text.replace(')',',')
            coords_text = re.findall(r'-?\d+(?:\.\d*)?', coords_text.rpartition(',')[0])
            coords_text = [float(i) for i in coords_text]
            coords = zip(*[iter(coords_text)] * 2)
            break
    latitudes = list()
    longitudes = list()
    for index in range(len(coords)):
        longitude, latitude = coords[index]
        latitudes.append(latitude)
        longitudes.append(longitude)
    return longitudes, latitudes

def insertapikey(fname, apikey):
    """put the google api key in a html file"""
    def putkey(htmltxt, apikey, apistring=None):
        """put the apikey in the htmltxt and return soup"""
        if not apistring:
            apistring = "https://maps.googleapis.com/maps/api/js?key=%s&callback=initMap"
        soup = BeautifulSoup(htmltxt, 'html.parser')
        body = soup.body
        src = apistring % (apikey, )
        tscript = soup.new_tag("script", src=src, async="defer")
        body.insert(-1, tscript)
        return soup
    htmltxt = open(fname, 'r').read()
    soup = putkey(htmltxt, apikey)
    newtxt = soup.prettify()
    open(fname, 'w').write(newtxt)

def plot_segment(segment_id, zoom):
    html_filename = str(segment_id) + '.html'
    gmaps = googlemaps.Client(key='AIzaSyDTZHhadJNoPuRPEyh1lRzTqxnEKfRv5Cg')
    longitudes, latitudes = get_coordinates_of_road_segment(segment_id)
    midpoint = len(latitudes)/2 - 1
    gmap = gmplot.GoogleMapPlotter(latitudes[midpoint], longitudes[midpoint], zoom)
    gmap.plot(latitudes, longitudes, 'red', edge_width=8)
    gmap.draw(html_filename)
    insertapikey(html_filename, 'AIzaSyDTZHhadJNoPuRPEyh1lRzTqxnEKfRv5Cg')
    return html_filename

def find_vehicles_based_on_start_end_segment(df, start, end):
    new_df = pd.DataFrame(columns = df.columns)
    for index, row in df.iterrows():
        if start == row.start_segment and end == row.end_segment:
            new_df.loc[len(new_df)] = row
    return new_df

def find_vehicles_using_road_segment(df, target_segment):
    new_df = pd.DataFrame(columns = df.columns)
    for index, row in df.iterrows():
        for segment in row.segments:
            new_df.loc[len(new_df)] = row
    return new_df

def find_vehicles_using_road_segment(df, target_segment):
    new_df = pd.DataFrame(columns = df.columns)
    for index, row in df.iterrows():
        if str(target_segment) in row.segments:
            new_df.loc[len(new_df)] = row
    return new_df

def find_vehicles_based_on_start_end_segment(df, start, end):
    new_df = pd.DataFrame(columns = df.columns)
    for index, row in df.iterrows():
        if start == row.start_segment and end == row.end_segment:
            new_df.loc[len(new_df)] = row
    return new_df